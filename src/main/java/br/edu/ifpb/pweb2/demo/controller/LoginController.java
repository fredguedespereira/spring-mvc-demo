package br.edu.ifpb.pweb2.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/login")
public class LoginController {
	
	@RequestMapping("/showLoginForm") 
	public String showLoginForm() {
		return "form-login";
	}
	
	@RequestMapping("/processLogin") 
	public String processLoginForm() {
		return "logged-in";
	}
	
	@RequestMapping("/validateLogin")
	public String validate(HttpServletRequest request, Model model) {
		String proxView = "form-login";
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		if (login.equals("fred") && senha.equals("123")) {
			model.addAttribute("usuario", login);
			proxView = "logged-in-2";
		}
		return proxView;
	}
	
	@RequestMapping("/validateLoginComParams")
	public String validate(
			@RequestParam("login") String login, 
			@RequestParam("senha") String senha,
			Model model) {
		String proxView = "form-login";
		if (login.equals("fred") && senha.equals("123")) {
			model.addAttribute("usuario", login);
			proxView = "logged-in-2";
		}
		return proxView;
	}
}
