package br.edu.ifpb.pweb2.demo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.edu.ifpb.pweb2.demo.dao.AlunoDAO;
import br.edu.ifpb.pweb2.demo.model.Aluno;

@Controller
@RequestMapping("/alunos")
public class AlunoController {
	
	@Autowired
	private AlunoDAO alunoDAO;
	
	@RequestMapping("/form")
	public String form(Model model) {
		model.addAttribute("aluno", new Aluno());
		return "alunos/form";
	}
	
	@RequestMapping(value="/cadastre", method=RequestMethod.POST)
	public String cadastre(Aluno aluno, Model model) {
		if (aluno.getId() == null) {
			alunoDAO.insert(aluno);
		} else {
			alunoDAO.update(aluno);
		}
		model.addAttribute("mensagem", "Aluno salvo com sucesso (id="+aluno.getId()+")!");
		model.addAttribute("aluno", new Aluno());
		return "alunos/form";
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public String busque(@PathVariable("id") Integer id, Model model) {
		Aluno aluno = alunoDAO.findById(id);
		if (aluno == null ) {
			model.addAttribute("mensagem", "Aluno com id="+id+" não encontrado!");
			model.addAttribute("aluno", new Aluno());
		} else {
			model.addAttribute("aluno", aluno);
		}
		return "alunos/form";
	}
	
	@ModelAttribute("paises")
	public List<String> getPaises() {
		return Arrays.asList(new String[] {"Brasil", "Argentina", "Bolívia", "Chile", "Colômbia", "Paraguai", "Peru"});
	}
	
	@ModelAttribute("linguagens")
	public List<String> getLinguagens() {
		return Arrays.asList(new String[] {"C", "Java", "Python", "Kotlin", "TypeScript"});
	}
	
	@ModelAttribute("linguas")
	public List<String> getLinguas() {
		return Arrays.asList(new String[] {"Português", "Espanhol", "Inglês", "Alemão", "Holandês"});
	}
}
